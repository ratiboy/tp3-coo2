from tp2 import *
from Thing import *

def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "bidule" not in b
    assert "truc2" in b

def test_box_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b
    assert "truc2" in b

def test_box_is_open():
    b = Box()
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()
    
def test_action_look():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert b.action_look() == "la boite est fermee"
    b.open()
    assert b.action_look() == "la boite contient: truc1, truc2"

def test_Thing_creation():
    chose = Thing(3,"chose")
    assert chose.getname() == "chose"
    assert chose.has_name("chose")
    assert chose.getvolume() == 3

def test_Box_capacity():
    b = Box()
    assert b.get_capacity() == None
    b.set_capacity(100)
    assert b.get_capacity() == 100
    assert b.get_capacity() != 1000

def test_has_room_for():
    b = Box()
    t = Thing(100000,"Baleine")
    assert b.has_room_for(t)
    b.set_capacity(1000)
    assert not b.has_room_for(t)
    
def test_action_add():
    b = Box()
    t1 = Thing(5,"Chose1")
    t2 = Thing(25,"Chose2")
    assert not b.action_add(t1)
    b.open()
    assert b.action_add(t1)
    assert b.action_add(t2)

