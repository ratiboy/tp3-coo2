class Box:
    def __init__(self):
        self._contents = []
        self._ouvert = False
        self._capacity = None
        

    def add(self,truc):
        self._contents.append(truc)

    def __contains__(self,machin):
        return machin in self._contents

    def remove(self,bidule):
        self._contents.remove(bidule)

    def is_open(self):
        return self._ouvert

    def open(self):
        self._ouvert = True

    def close(self):
        self._ouvert = False

    def action_look(self):
        if not self.is_open():
            return "la boite est fermee"
        else :
            return "la boite contient: "+', '.join(self._contents)

    def get_capacity(self):
        return self._capacity

    def set_capacity(self,capacity):
        self._capacity = capacity

    def has_room_for(self,truc):
        return self._capacity is None or self._capacity >= truc.getvolume()

    def action_add(self,truc):
        if self._ouvert and self.has_room_for(truc):
            self.add(truc)
            if self.get_capacity() is not None :
                self._capacity -= truc.getvolume()
            return True
        return False

    def __repr__(self):
        return "Thing("+str(self.getvolume())+","+self.getname()+")"
        

    