class Thing:
    def __init__(self,volume,name):
        self._name = name
        self._volume = volume

    def getvolume(self):
        return self._volume

    def getname(self):
        return self._name 

    def has_name(self,nom):
        return self.getname() == nom